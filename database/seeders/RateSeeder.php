<?php

namespace Database\Seeders;

use App\DTO\RateDTO;
use App\Services\Api\RateService;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class RateSeeder extends Seeder
{

    // currency field has default value RUB
    protected array $rates = [
        [
            'title' => 'Старт',
            'price' => 550,
            'speed' => 100
        ],
        [
            'title' => 'Старт + видеонаблюдение',
            'price' => 800,
            'speed' => 100
        ],
        [
            'title' => 'Взлет',
            'price' => 700,
            'speed' => 330
        ],
        [
            'title' => 'Сияние',
            'price' => 900,
            'speed' => 530
        ],
        [
            'title' => 'Космос',
            'price' => 1100,
            'speed' => 730
        ],
    ];

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $service = App::make(RateService::class);

        foreach ($this->rates as $rate) {
            $service->store(new RateDTO($rate));
        }
    }
}
