<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
</head>
<body>
    <div class="container">
        <table class="table table-hover table-bordered">
            <thead>
            <tr>
                <th class="col">Title</th>
                <th class="col">Price</th>
                <th class="col">Currency</th>
                <th class="col">Speed</th>
            </tr>
            </thead>
            <tbody>
                @foreach($rates as $rate)
                    <tr>
                        <td>{{ $rate->title }}</td>
                        <td>{{ $rate->price }}</td>
                        <td>{{ $rate->currency }}</td>
                        <td>{{ $rate->speed }} <span style="cursor:pointer" data-id="{{$rate->id}}"
                                                     data-bs-toggle="modal" data-bs-target="#rate-edit-modal"><i
                                    class="fa fa-pen"></i></span></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="modal fade" tabindex="-1" id="rate-edit-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Enter new rate speed</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id">
                    <input type="number" class="form-control" id="speed">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="saveBtn">Save changes</button>
                </div>
            </div>
        </div>
    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <script>
        (function($) {
            var modal = document.getElementById('rate-edit-modal')
            modal.addEventListener('show.bs.modal', function (event) {
                $("#id").val($(event.relatedTarget).data('id'))
            });

            $("#saveBtn").click(function() {
                var url = "/api/rates/" + $("#id").val()

                $.ajax({
                    url,
                    type: "PUT",
                    data: {
                        speed: $("#speed").val()
                    },
                }).done(function() {
                    $("#id").val(null)
                    $("#speed").val(null)
                    $("#rate-edit-modal").modal("toggle")
                    window.location.reload()
                    alert("Updated")
                }).fail(function() {
                    alert("Error")
                });
            });
        })(jQuery)
    </script>
</body>
</html>
