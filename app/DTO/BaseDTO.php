<?php

namespace App\DTO;

use App\Interfaces\DTO\BaseDTOInterface;
use JetBrains\PhpStorm\NoReturn;

abstract class BaseDTO implements BaseDTOInterface
{
    public ?int $id = null;

    #[NoReturn] public function __construct()
    {
        $this->applyFields(func_get_args()[0]);
    }

    #[NoReturn] public function applyFields(array $fields): void
    {
        $rc = new \ReflectionClass($this);

        foreach ($fields as $field => $value) {
            if (property_exists($this, $field)) {
                $variableType = $rc->getProperty($field)->getType()->getName();

                if (class_exists($variableType)) {
                    $this->$field = new $variableType($value);
                } else {
                    $this->$field = $value;
                }
            }
        }

        $validated = $this->validate();

        if (!$validated['success']) {
            throw new \Exception(sprintf("The following fields can not be null: %s", implode(', ', $validated['errors'])));
        }
    }

    public function validate(): array
    {
        $rc = new \ReflectionClass($this);
        $properties = $this->toArray();
        $errors = [];

        foreach ($properties as $key => $value) {
            if ($rc->hasProperty($key)) {
                $property = $rc->getProperty($key);

                if (!$property->getType()->allowsNull() && !isset($this->$key)) {
                    $errors[] = $key;
                }
            }
        }

        return [
            'success' => count($errors) === 0,
            'errors' => $errors
        ];
    }

    public function toArray(): array
    {
        $vars = get_class_vars(static::class);
        $array = [];

        foreach ($vars as $var => $empty) {
            $array[$var] = $this->$var;
        }

        return $array;
    }
}
