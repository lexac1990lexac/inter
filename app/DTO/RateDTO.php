<?php

namespace App\DTO;

use App\Enums\Currency;

class RateDTO extends BaseDTO
{
    public string $title;

    public float $price;

    public ?string $currency = null;

    public int $speed;

    public function toArray(): array
    {
        return [
            'title' => $this->title,
            'price' => $this->price,
            'currency' => $this->currency ?? Currency::RUB->value,
            'speed' => $this->speed
        ];
    }
}
