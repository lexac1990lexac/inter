<?php

namespace App\Services\Base;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;

final class ServiceOutput
{
    private bool $success = false;

    private ?string $message = null;

    private array $additionalKeys = [];

    public function isSuccess(): bool
    {
        return $this->success === true;
    }

    public function isFailure(): bool
    {
        return $this->success === false;
    }

    public function getSuccess(): bool
    {
        return $this->success;
    }

    public function setSuccess(bool $success): ServiceOutput
    {
        $this->success = $success;

        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): ServiceOutput
    {
        $this->message = $message;

        return $this;
    }

    public static function fromArray(array $array): ServiceOutput
    {
        $instance = new self();

        $instance->setSuccess($array['success'] ?? false)
            ->setMessage($array['message'] ?? null);

        foreach (Arr::except($array, ['success', 'message']) as $field => $value) {
            $instance->$field = $value;
        }

        return $instance;
    }

    public function __set(string $name, $value): void
    {
        $this->additionalKeys[$name] = $value;
    }

    public function __get(string $name)
    {
        return $this->additionalKeys[$name];
    }

    public function __call(string $name, array $arguments)
    {
        if (!method_exists($this, $name)) {
            if (Str::startsWith($name, 'get')) {
                $keyName = Str::lcfirst(Str::replace('get', '', $name));

                return $this->$keyName;
            }
        }
    }
}
