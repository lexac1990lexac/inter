<?php

namespace App\Services\Api;
use App\DTO\RateDTO;
use App\Repository\RateRepository;
use App\Services\Base\BaseService;
use App\Services\Base\ServiceOutput;

class RateService extends BaseService
{
    public function __construct(
        private readonly RateRepository $repository
    )
    {
    }

    public function store(RateDTO $rateDTO): ServiceOutput
    {
        $rate = $this->repository->store($rateDTO->toArray());

        return (new ServiceOutput())->setSuccess(isset($rate))
            ->setMessage("Rate successfully created");
    }

    public function update(RateDTO $rateDTO): ServiceOutput
    {
        $updatedRate = $this->repository->update($rateDTO->id, $rateDTO->toArray());

        return (new ServiceOutput())->setSuccess(isset($updatedRate))
            ->setMessage("Rate successfully updated");
    }

    public function destroy(int $rateId): ServiceOutput
    {
        return (new ServiceOutput())->setSuccess($this->repository->delete($rateId))
            ->setMessage("Rate successfully deleted");
    }
}
