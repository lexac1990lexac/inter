<?php

namespace App\Repository;

use App\Models\Rate;

class RateRepository extends BaseRepository
{

    public function getModel(): string
    {
        return Rate::class;
    }
}
