<?php

namespace App\Repository;

use App\Interfaces\CRUD\CRUDInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository implements CRUDInterface
{
    public function all(): Collection
    {
        $query = $this->query();

        return $query->get();
    }

    public function first(): ?Model
    {
        $query = $this->query();

        return $query->first();
    }

    public function show(int $id): ?Model
    {
        $query = $this->query();

        return $query->find($id);
    }

    public function store(array $data)
    {
        $query = $this->query();

        if($query->where($data)->exists()) {
            return $query->where($data)->first();
        }

        $newInstance = $query->getModel();

        $newInstance->fill($data);

        $newInstance->save();

        return $newInstance;
    }

    public function delete(int $id): bool
    {
        $query = $this->query();

        return $query->where('id', $id)->delete();
    }

    public function update(int $id, array $data)
    {
        $query = $this->query();

        $existedInstance = $query->find($id);

        $existedInstance->fill($data);

        $existedInstance->save();

        return $existedInstance;
    }

    public function query(): Builder
    {
        $model = $this->getModel();

        return $model::query();
    }

    abstract public function getModel(): string;
}
