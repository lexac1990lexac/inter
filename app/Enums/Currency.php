<?php

namespace App\Enums;

use App\Traits\EnumToArray;

enum Currency: string
{
    use EnumToArray;

    // 4 main currencies in my country
    case KZT = "KZT";
    case RUB = "RUB";
    case USD = "USD";
    case EUR = "EUR";
}
