<?php

namespace App\Http\Controllers;

use App\Services\Base\ServiceOutput;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public function responseMessageFromServiceOutput(ServiceOutput $output): JsonResponse
    {
        $status = $output->isSuccess() ? ResponseAlias::HTTP_OK : ResponseAlias::HTTP_BAD_REQUEST;

        return response()->json([
            'message' => $output->getMessage()
        ], $status);
    }
}
