<?php

namespace App\Http\Controllers\Api;

use App\DTO\RateDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Rate\StoreRequest;
use App\Http\Requests\Api\Rate\UpdateRequest;
use App\Http\Resources\Api\Rate\RateCollection;
use App\Http\Resources\Api\Rate\RateResource;
use App\Models\Rate;
use App\Repository\RateRepository;
use App\Services\Api\RateService;
use Illuminate\Http\JsonResponse;

class RatesController extends Controller
{
    public function __construct(
        private readonly RateRepository $repository,
        private readonly RateService    $service
    )
    {
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): RateCollection
    {
        return new RateCollection($this->repository->all());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request): JsonResponse
    {
        $output = $this->service->store(new RateDTO($request->validated()));

        return $this->responseMessageFromServiceOutput($output);
    }

    /**
     * Display the specified resource.
     */
    public function show(Rate $rate): RateResource
    {
        return new RateResource($rate);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, Rate $rate): JsonResponse
    {
        $output = $this->service->update(new RateDTO([...$rate->getAttributes(), ...$request->validated()]));

        return $this->responseMessageFromServiceOutput($output);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Rate $rate): JsonResponse
    {
        $output = $this->service->destroy($rate->getKey());

        return $this->responseMessageFromServiceOutput($output);
    }
}
