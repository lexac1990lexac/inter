<?php

namespace App\Http\Controllers;

use App\Repository\RateRepository;
use Illuminate\Http\Request;

class RateController extends Controller
{
    public function __construct(
        private readonly RateRepository $repository
    )
    {
    }

    public function index(): string
    {
        $rates = $this->repository->all();

        return view('welcome', compact('rates'));
    }
}
