<?php

namespace App\Interfaces\DTO;

interface BaseDTOInterface
{
    public function toArray(): array;

    public function applyFields(array $fields): void;

    public function validate(): array;
}
