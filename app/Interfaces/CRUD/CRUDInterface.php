<?php

namespace App\Interfaces\CRUD;

use App\Interfaces\CRUD\Defaults\DeleteInterface;
use App\Interfaces\CRUD\Defaults\ShowInterface;
use App\Interfaces\CRUD\Defaults\StoreInterface;
use App\Interfaces\CRUD\Defaults\UpdateInterface;

interface CRUDInterface extends StoreInterface, ShowInterface, UpdateInterface, DeleteInterface
{

}
