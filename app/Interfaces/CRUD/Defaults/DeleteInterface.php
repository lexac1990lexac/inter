<?php

namespace App\Interfaces\CRUD\Defaults;

interface DeleteInterface
{
    public function delete(int $id);
}
