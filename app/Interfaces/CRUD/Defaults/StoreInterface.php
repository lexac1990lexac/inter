<?php

namespace App\Interfaces\CRUD\Defaults;

interface StoreInterface
{
    public function store(array $data);
}
