<?php

namespace App\Interfaces\CRUD\Defaults;

interface ShowInterface
{
    public function show(int $id);
}
