<?php

namespace App\Interfaces\CRUD\Defaults;

interface UpdateInterface
{
    public function update(int $id, array $data);
}
