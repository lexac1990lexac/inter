Для запуска приложения потребуется [docker](https://docs.docker.com/engine/install/)

Для начала соберём контейнер через docker-compose

<code>docker-compose build app</code>

После завершения выполнения первой команды, нужно запустить созданные контейнеры

<code>docker-compose up -d</code>

После непродолжительного ожидания запустятся 3 контейнера(app,db,nginx).
<p style="font-style:italic">(Запуск db займёт +-2 минуты, тут нужно подождать)</p>

Далее нужно выполнить команды по созданию .env файла, установке зависимостей приложения(пакетов), а также создания
таблиц и их наполнения

<p><code>docker-compose exec app mv .env.example .env</code></p>
<p><code>docker-compose exec app composer install</code></p>
<p><code>docker-compose exec app php artisan migrate --seed</code></p>

После всех этих действий можно будет перейти в приложение [ТЫК](http://localhost:8000)

P.s. Я не парился насчёт скорости запросов к приложению в докер-контейнере, лично на моём ПК запросы проходят
медленнее, чем если бы приложение работало через OpenServer.

Можно развернуть приложение без docker'а, тут нужны будут только последних команды без `docker-compose exec app` части
